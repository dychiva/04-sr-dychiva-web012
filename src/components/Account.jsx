import React from "react";
import queryString from "query-string";
import { Link } from "react-router-dom";

function Account(props) {
  let nameUrl = queryString.parse(props.location.search);

  return (
    <div className="container" style={{ padding: "4%" }}>
      <h1>Account</h1>
      <Link to={`/Account?name=dyvannak`}>2. Dy Vannak</Link>
      <br />
      <Link to={`/Account?name=dychiva`}>1. Dy Chiva</Link>
      <br />
      <Link to={`/Account?name=seansovanveasna`}>3. Sean Sovanveasna</Link>
      <br />
      <Link to={`/Account?name=vandymonyratana`}>4. Vandy Monyratana</Link>
      <br />
      <Link to={`/Account?name=dyrivit`}>5. Dy Rivit</Link>

      {nameUrl.name === undefined ? (
        <h2>There is no name in the querystring</h2>
      ) : (
        <h3>
          The <span style={{ color: "red" }}>name</span> in the query string is
          "{nameUrl.name}"{" "}
        </h3>
      )}
    </div>
  );
}

export default Account;
