import React from "react";
import { Navbar, Nav, Form, FormControl, Button } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { Link } from "react-router-dom";

export default function Menu() {
  return (
    <div>
      <Navbar bg="primary" variant="dark">
        <Navbar.Brand as={Link} to="/">
          React Router
        </Navbar.Brand>
        <Nav className="mr-auto">
          <Nav.Link as={Link} to="/Home">
            Home
          </Nav.Link>
          <Nav.Link as={Link} to="/Vedio">
            Vedio
          </Nav.Link>
          <Nav.Link as={Link} to="/Account">
            Account
          </Nav.Link>
          <Nav.Link as={Link} to="/Auth">
            Auth
          </Nav.Link>
        </Nav>
        <Form inline>
          <FormControl type="text" placeholder="Search" className="mr-sm-2" />
          <Button variant="outline-light">Search</Button>
        </Form>
      </Navbar>
    </div>
  );
}
